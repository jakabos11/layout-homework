import { Component, EventEmitter, Output } from '@angular/core';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {

  @Output() likepressed:EventEmitter<boolean> = new EventEmitter();

  LikePressed(){
    this.likepressed.emit(true);
  }

}
