import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-snow',
  templateUrl: './snow.component.html',
  styleUrls: ['./snow.component.scss'],
})
export class SnowComponent {
  @Input() snowText: string = '';
  counter(i: number) {
    return new Array(i);
  }
}
