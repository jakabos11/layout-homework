import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent {
  loading: boolean = false;

  constructor(private _snackBar: MatSnackBar) {}

  openSnackBar(message: string) {
    this._snackBar.open(message, '', {
      duration: 4000,
      horizontalPosition: 'left',
    });
  }

  showProgressBar() {
    this.loading = true;

    setTimeout(() => {
      this.loading = false;
      this.openSnackBar("Hi, I'm your snackbar you've been waiting for 🐥");
    }, 4000);
  }
}
